from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Project(models.Model):
    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Project list"

    STATUSES = (
        ('to_do', 'В работе'),
        ('in_stable', 'Стабилизация'),
        ('release', 'Выпуск'),
        ('done', 'Готово'),
        ('close', 'Закрыто')
    )

    title = models.CharField("title", max_length=200)
    description = models.TextField("description", max_length=2000, null=True, blank=True)
    created_at = models.DateTimeField("created at", auto_now_add=True)
    last_modified = models.DateTimeField("last modified", auto_now=True, editable=False)
    deadline = models.DateField("deadline", null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_created', verbose_name='created by',
                                   on_delete=models.SET_NULL, null=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_modified',
                                    verbose_name='modified by', on_delete=models.SET_NULL, null=True)
    state = models.CharField("state", max_length=20, choices=STATUSES, default='to_do')
    git = models.CharField("git", max_length=100, )


class Mark(models.Model):
    class Meta:
        verbose_name = "Mark"
        verbose_name_plural = "Mark list"

    STATUSES = (
        ('to_do', 'В работе'),
        ('in_stable', 'Стабилизация'),
        ('release', 'Выпуск'),
        ('done', 'Готово'),
        ('close', 'Закрыто')
    )

    title = models.CharField("title", max_length=200)
    description = models.TextField("description", max_length=2000, null=True, blank=True)
    state = models.CharField("state", max_length=20, choices=STATUSES, default='to_do')
    created_at = models.DateTimeField("created at", auto_now_add=True)
    last_modified = models.DateTimeField("last modified", auto_now=True, editable=False)
    deadline = models.DateField("deadline", null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='mark_created', verbose_name='created by',
                                   on_delete=models.SET_NULL, null=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='mark_modified',
                                    verbose_name='modified by', on_delete=models.SET_NULL, null=True)


class Task(models.Model):
    class Meta:
        verbose_name = "Task"
        verbose_name_plural = "Tasks"

    STATUSES = (
        ('to_do', 'В работе'),
        ('in_test', 'На тест'),
        ('in_checking', 'На проверку'),
        ('done', 'Готово'),
        ('close', 'Закрыто')
    )

    PRIORITIES = (
        ('00_low', 'Low'),
        ('10_normal', 'Normal'),
        ('20_high', 'High'),
        ('30_critical', 'Critical'),
        ('40_blocker', 'Blocker')
    )

    title = models.CharField("title", max_length=200)
    description = models.TextField("description", max_length=2000, null=True, blank=True)
    resolution = models.TextField("resolution", max_length=2000, null=True, blank=True)
    deadline = models.DateField("deadline", null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='tasks_assigned', verbose_name='assigned to',
                             on_delete=models.SET_NULL, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='tasks_created', verbose_name='created by',
                                   on_delete=models.SET_NULL, null=True)
    state = models.CharField("state", max_length=20, choices=STATUSES, default='to_do')
    priority = models.CharField("priority", max_length=20, choices=PRIORITIES, default='10_normal')
    created_at = models.DateTimeField("created at", auto_now_add=True, editable=False)
    last_modified = models.DateTimeField("last modified", auto_now=True, editable=False)

    def __str__ (self):
        return "[%s] %s" % (self.id, self.title)


class MarkProject(models.Model):
    class Meta:
        verbose_name = "Mark_project"
        verbose_name_plural = "Mark_project"

    id_mark = models.ForeignKey(Mark, related_name='mark_id', verbose_name='mark id', on_delete=models.CASCADE)
    id_project = models.ForeignKey(Project, related_name='project_id',
                                   verbose_name='project id', on_delete=models.CASCADE)


class MarkTask(models.Model):
    class Meta:
        verbose_name = "Mark_task"
        verbose_name_plural = "Mark_task"

    id_mark = models.ForeignKey(Mark, related_name='mark_id', verbose_name='mark id', on_delete=models.CASCADE)
    id_task = models.ForeignKey(Task, related_name='task_id',
                                verbose_name='task id', on_delete=models.CASCADE)


class Item(models.Model):
    class Meta:
        verbose_name = "Item"
        verbose_name_plural = "Check List"

    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    item_description = models.CharField("description", max_length=200)
    is_done = models.BooleanField("done?", default=False)

    def __str__ (self):
        return self.item_description
