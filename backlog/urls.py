from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.http import HttpResponseRedirect

urlpatterns = [
    url(r'^$', lambda r: HttpResponseRedirect('admin/')),   # Remove this redirect if you add custom views
    url(r'^admin/', admin.site.urls),
]

admin.site.site_header = settings.SITE_HEADER
